package Advance;
import robocode.*;
import java.awt.Color;
import java.lang.*;

public class Advanced extends AdvancedRobot
{
	
	public void run() {
		
		while(true) {
			setTurnRadarRight(360);
			setTurnRight(40);
			setAhead(80);
			execute();

		}
	}
	public void aim(double enemy) {
		double A=getHeading()+enemy-getGunHeading();
		if (!(A > -180 && A <= 180)) {
			while (A <= -180) {
				A += 360;
			}
			while (A > 180) {
				A -= 360;
			}
		}
		turnGunRight(A);
	}
	
	public void onScannedRobot(ScannedRobotEvent scann) {
		double distancia = scann.getDistance();
		aim(scann.getBearing());
		if (distancia >= 600) {
			if (getEnergy() >= 80) {
				fire(0.75);
			} else if (getEnergy() >= 54.6 && getEnergy() < 80) {
				fire(0.5);
			} else if (getEnergy() >= 28 && getEnergy() < 54.6) {
				fire(0.25);
			}
		} else if (distancia >= 400 && distancia <600) {
			if (getEnergy() >= 80) {
				fire(1.75);
			} else if (getEnergy() >= 54.6 && getEnergy() < 80) {
				fire(1.17);
			} else if (getEnergy() >= 28 && getEnergy() < 54.6) {
				fire(0.59);
			}
		} else if (distancia >=200 && distancia < 400) {
			if (getEnergy() >= 80) {
				fire(2.75);
			} else if (getEnergy() >= 54.6 && getEnergy() < 80) {
				fire(1.84);
			} else if (getEnergy() >= 28 && getEnergy() < 54.6) {
				fire(0.93);
			}
		} else if (distancia < 200) {
			if (getEnergy() >= 80) {
				fire(3);
			} else if (getEnergy() >= 54.6 && getEnergy() < 80) {
				fire(2);
			} else if (getEnergy() >= 28 && getEnergy() < 54.6) {
				fire(1);
			} 
		}
		
	}
	
	public void onHitWall(HitWallEvent hitwall){
    	double bearing = hitwall.getBearing();
	    setTurnRight(-bearing/1.5);
	    setAhead(100);
	}
	
	public void onHitRobot(HitRobotEvent colisao) {
		double ang = colisao.getBearing();
		if (ang >= 180 && ang <=360) {
			turnRight(180);
		} else if (ang < 180 && ang >= 0) {
			turnLeft(180);
		}
		aim(colisao.getBearing());
		ahead(40);	
	}
	
	public void onWin(WinEvent win) {
		for (int i = 0; i < 50; i++) {
			turnRight(30);
			turnLeft(30);
		}
	}
	
	
}
