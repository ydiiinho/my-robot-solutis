## RoboCodeSolutis

- Robô: Advanced
- Piloto: Edilson da Silva Almeida Filho

## Pontos Fortes

- Cálculo de Energia por Tiro;
- Cálculo de Distância do Oponente;
- Fácil de sair de colisões com paredes.

### Pontos Fracos

- Movimentação pouco fluida;
- Dificuldade quando há combate corpo a corpo.

# Aprendizado

A experiência de criar este robô foi incrível, desde o primeiro dia me vejo curioso a testar mais e mais funções atrás de resultados, estes hora positivos, hora negativos, mas a cada final de ciclo uma emoção de ter saído dali não só com mais conhecimento, mas com ainda mais vontade de resolver problemas. Tive a chance de não só me prender ao RoboCode, mas mergulhar novamente na matemática e até um pouco de física. Revisei tanto trigonometria que até ao dormir podia ver como ela poderia se relacionar com o RoboCode, mas sou um programador ainda inexperiente, apesar das milhares de ideias, colocar em prática na linguagem da máquina são outros quinhentos, me falta certas ferramentas, ferramentas estas que desejo poder estar aprendendo durante uma possível oportunidade com vocês da equipe Solutis. Agradeço desde já.
